all: src/main.c src/student.c src/student.h
	gcc -g -o build/student src/main.c src/student.c -I src/ -lsqlite3

clean:	build/student
	rm build/student
