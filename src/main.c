#include <stdio.h>
#include <student.h>

int main(int argc, char *argv[])
{
  Student s = Student__create("Ayush Jha", 15707);
  
  Student__set_name(s, "Dikshant Jha");
  
  printf("Student Name : %s\nStudent Roll No : %d\n",
        Student__get_name(s),
        Student__get_roll_no(s));
  Student__set_db(s, "student.db");
  Student__save(s);
  return 0;
}
