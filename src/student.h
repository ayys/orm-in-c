struct student;

typedef struct student *Student;

Student 
Student__create(char* name, unsigned int roll_no);


char * Student__get_name(Student self);
void Student__set_name(Student self, char * name);

unsigned int Student__get_roll_no(Student self);
void Student__set_roll_no(Student self, unsigned int roll_no);

void Student__set_db(Student self, char *db_filename);

int Student__save(Student self);
