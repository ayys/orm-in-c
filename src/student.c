#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sqlite3.h>
#include <student.h>
#include <db.h>

// Copy the folowing struct to student.c
struct student {
  DB_INTERFACE;
  char* name;
  unsigned int roll_no;
};

Student Student__create(char* name, unsigned int roll_no)
{
  Student self = malloc(sizeof(Student));
  self->name = malloc(strlen(name) + 1);
  strcpy(self->name, name);
  self->roll_no = roll_no;
  return self;
}

char * Student__get_name(Student self)
{
  return self->name;
}

void Student__set_name(Student self, char * name)
{
  size_t name_size = strlen(name) * sizeof(char);
  
  self->name = malloc(name_size);
  memset(self->name, 0, name_size);
  strcpy(self->name, name);
}

unsigned int Student__get_roll_no(Student self)
{
  return self->roll_no;
}

void Student__set_roll_no(Student self, unsigned int roll_no)
{
  self->roll_no = roll_no;
}


void Student__set_db(Student self, char *db_filename)
{
  self->rc = sqlite3_open(db_filename, &(self->db));
}


int Student__save(Student self)
{
  char sql[512];
  char *err_msg;
  if (self->rc)
    {
      return -1;
    }
  else
    {
      sprintf(sql, "INSERT INTO student (name, roll_no) VALUES (\"%s\", %d)", self->name, self->roll_no);
      sqlite3_exec(self->db, sql, 0, 0, &err_msg);
    }
}
